//imports
import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js';
import { getAuth, createUserWithEmailAndPassword, signInWithEmailAndPassword } from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-auth.js';
import { getDatabase, ref, set} from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-database.js'

//firebase keys
const firebaseConfig = {
    apiKey: "AIzaSyDD41AsOVXm8RrJPJmQ-2IzrBjFhTf7Zqg",
    authDomain: "proyectoforo-b8436.firebaseapp.com",
    databaseURL: "https://proyectoforo-b8436-default-rtdb.firebaseio.com/",
    projectId: "proyectoforo-b8436",
    storageBucket: "proyectoforo-b8436.appspot.com",
    messagingSenderId: "743344245619",
    appId: "1:743344245619:web:ef82cfb32a9097b0b79306",
  };
//firebase inicialization 
const app = initializeApp(firebaseConfig);
//database startup
const database = getDatabase(app);

//html register references
var RMailRef = document.getElementById("RMailId")
var RPassRef = document.getElementById("RPasswordId")
var RRPassRef = document.getElementById("RRPasswordId")
var RAKARef = document.getElementById("AKAId")
var RCountryRef = document.getElementById("CountryId")
var RButtonRef = document.getElementById("RButtonId")

//html login references
var LMailRef = document.getElementById("LMailId")
var LPassRef = document.getElementById("LPasswordId")
var LButtonRef = document.getElementById("LButtonId")

//event listeners
RButtonRef.addEventListener("click", creation);
LButtonRef.addEventListener("click", login);

//autentification
const auth = getAuth();

//functions
function creation(){
    if ((RMailRef.value != '') && (RPassRef.value != '') && (RRPassRef.value != '') && (RAKARef.value != '') && (RCountryRef.value != '')){
        if(RPassRef.value != RRPassRef.value) {
            alert("Check if the passwords are the same");
        }
        else {
            createUserWithEmailAndPassword(auth, RMailRef.value, RPassRef.value)
            .then((userCredential) => {
            // Signed in
            const user = userCredential.user;
            const useruid = user.uid;
            alert("User succesfully created");
            WriteUserData(useruid);
            // ...
            })

            .catch((error) => {
            const errorCode = error.code;
            const errorMessage = error.message;
            alert(errorMessage)
            if(errorCode == 'auth/email-already-in-use'){
                alert("Mail already in use");
            }
            // ..
            });

        }
    }
    else {
        alert("Complete blank spaces")
    }
}

function login(){
    if((LMailRef.value != '') && (LPassRef.value != '')){
        signInWithEmailAndPassword(auth, LMailRef.value, LPassRef.value)
        .then((userCredential) => {
            // Signed in
            const user = userCredential.user;
            window.location.href = "../pages/Homelogged.html"
            // ...
        })
        .catch((error) => {
            const errorCode = error.code;
            const errorMessage = error.message;
            alert(errorMessage)
        });
    }
    else{
        alert("Complete blank spaces")
    }
}

function WriteUserData(uid) // Guarda en la base de datos los datos del Usuario
{
    let WMail = RMailRef.value
    let WAKA = RAKARef.value
    let WCountry = RCountryRef.value
    const UserData = {
        uid:  uid,
        Name: WAKA,
        Email: WMail, 
        Country: WCountry
      };
      //Reestart ref values
    RMailRef.value = ""
    RPassRef.value = ""
    RRPassRef.value = ""
    RAKARef.value = ""
    RCountryRef.value = ""

    set(ref(database, "users/" + uid), {
        UserData
    })
    alert("It seems we got a new user");
}
