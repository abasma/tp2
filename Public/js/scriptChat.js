//imports
import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js'
import { getAuth, onAuthStateChanged, signOut} from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-auth.js'
import { getDatabase, ref, set, onValue, push, query, orderByChild, child, get} from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-database.js'

//firebase keys
const firebaseConfig = {
  apiKey: "AIzaSyDD41AsOVXm8RrJPJmQ-2IzrBjFhTf7Zqg",
  authDomain: "proyectoforo-b8436.firebaseapp.com",
  databaseURL: "https://proyectoforo-b8436-default-rtdb.firebaseio.com/",
  projectId: "proyectoforo-b8436",
  storageBucket: "proyectoforo-b8436.appspot.com",
  messagingSenderId: "743344245619",
  appId: "1:743344245619:web:ef82cfb32a9097b0b79306",
};
//firebase inicialization 
const app = initializeApp(firebaseConfig);
//auth
const auth = getAuth();
//cheking current user
const user = auth.currentUser;
//database
const database = getDatabase();

var mensajeRef = document.getElementById("textoChatId");
var chatRef = document.getElementById("mensajeChatId");

// Reference chat btn and eventlistener
var enviarRef = document.getElementById("buttonChatId");
enviarRef.addEventListener("click", enviarMensaje);

// Reference logout btn and eventlistener
var logOut = document.getElementById("logoutBtn");
logOut.addEventListener("click", loggingOut);

let uid;
let correo;

onAuthStateChanged(auth, (user) => {
    if (user) {
      // Mail and UID from logged user
      uid = user.uid;
      correo = user.email;

    } else {
    }
});


function enviarMensaje(){
    let mensaje = mensajeRef.value;

    push(ref(database, "mensaje/"), {
        msg: mensaje,
        email: correo 
    })

    mensajeRef.value = "";
    console.log("Mensaje enviado. Correo: " + correo);
}

const queryMensajes = query(ref(database, 'mensaje/'), orderByChild('email'));
console.log(queryMensajes);

onValue(queryMensajes, (snapshot) => {
    chatRef.innerHTML = '';
    snapshot.forEach((childSnapshot) => {
      const childKey = childSnapshot.key;
      const childData = childSnapshot.val().msg;
      console.log(childData);
      
      chatRef.innerHTML += `
          <p>${childSnapshot.val().email}: ${childSnapshot.val().msg}</p>
      `;
    });
});

function loggingOut(){

  const auth = getAuth();
signOut(auth).then(() => {
  window.location.href ="./Home.html";
}).catch((error) => {
});

}

